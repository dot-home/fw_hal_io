/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/

#ifndef _HAL_IO_H_
#define _HAL_IO_H_

#ifdef __cplusplus
extern "C"
{
#endif   

#include "BaseTypes.h"
#include "HAL_Config.h"

/****************************************** Defines ******************************************************/
typedef enum
{
    #ifdef ISR_IO_MAP
        #define ISR_MAP(ePort, pfPin_0, pfPin_1, pfPin_2, pfPin_3, pfPin_4, pfPin_5, pfPin_6, pfPin_7) ePort,
            ISR_IO_MAP
        #undef ISR_MAP
    #endif
    eInvalidPort
}teIoISR;

typedef enum
{
    #ifdef PWM_MAP
        #define P_MAP(PwmName, PwmFn) PwmName,
            PWM_MAP
        #undef P_MAP
    #endif
    eInvalidPWM    
}tePWM;

//Enum for the Senselines
typedef enum
{
    #ifdef SENSE_MAP
    #define S_MAP( Senseline, PinMapping) Senseline,
        SENSE_MAP
    #undef S_MAP
    #endif
    eInvalidSense
}teSenseLine;


//Enum for the outputs
typedef enum
{
    #ifdef OUTPUT_MAP
    #define O_MAP( Senseline, PinMapping) Senseline,
        OUTPUT_MAP
    #undef O_MAP
    #endif
    eInvalidOutput
}teOutput;

typedef enum
{
    eIO_Success,        /* Operation was successful */
    eIO_InvalidPWM,     /* Chosen PWM-Module is invalid */
    eIO_InvalidOutput,  /* Chosen Output is invalid */
    eIO_InvalidInput,   /* Chosen Input is invalid */
    eIO_InvalidPointer, /* Given pointer is invalid */
    eIO_InvalidValue,   /* Used value is invalid */
    eIO_Invalid         /* Can't determine fault. Set to invalid */
}teIO_Return;

// struct for invalid ports and pins
typedef struct
{
    teOutput eOutputFaultList[OUTPUT_FAULT_COUNT];
    u8  ucFaultCnt;
    u8  ucPinFaults[NUMBER_OF_PORTS];
}tsFaultOutputPortValues;

/****************************************** External visible functiones **********************************/
void HAL_IO_Init(void);

#if COUNT_OF_SENSELINES
    bool HAL_IO_ReadDigitalSense(teSenseLine eSenseLine);
#endif //COUNT_OF_SENSELINES

#if COUNT_OF_OUTPUTS
    bool HAL_IO_ReadOutputStatus(teOutput eOutput);
    void HAL_IO_SetOutputStatus(teOutput eOutput, bool bStatus);
#endif //COUNT_OF_OUTPUTS

u8   HAL_IO_ReadPortStateRegister(u8 ucPort);
u8   HAL_IO_ReadPortDataRegister(u8 ucPort);
void HAL_IO_GetPortAndBitshiftIndex(teOutput eOutput, u8* pucPortIdx, u8* pucBitshiftIdx);

bool HAL_IO_GetPortPinMaskStatus(void);
void HAL_IO_Sleep(void);
void HAL_IO_Wakeup(void);
bool HAL_IO_GetPwmStatus(u8 ucOutputIdx);

u8*  HAL_IO_GetOutputPinMask(void);

#if COUNT_OF_PWM
    teIO_Return HAL_IO_PWM_Start(tePWM ePWM);
    teIO_Return HAL_IO_PWM_Stop(tePWM ePWM);
    teIO_Return HAL_IO_PWM_ReadCompare(tePWM ePWM, u16* puiCompVal);
    teIO_Return HAL_IO_PWM_WriteCompare(tePWM ePWM, u32 ulCompVal);
    teIO_Return HAL_IO_PWM_ReadPeriod(tePWM ePWM, u16* puiPeriodVal);
    teIO_Return HAL_IO_PWM_WritePeriod(tePWM ePWM, u32 ulPeriodVal);
#endif //COUNT_OF_PWM

#ifdef __cplusplus
}
#endif    

#endif //_HAL_IO_H_