/* ========================================
 *
 * Copyright Eduard Kraemer, 2019 *
 * ========================================
*/

#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

#include "HAL_IO.h"
#include "project.h"

/****************************************** Defines ******************************************************/
typedef struct _pwm_fn_mapping
{
    pFunction pfnStart;
    pFunction pfnStop;
    pFunctionParamU32 pfnWriteCompare;
    pulFunction pfnReadCompare;
    pulFunction pfnReadPeriod;
    pFunctionParamU32 pfnWritePeriod;
    u32 ulMaxPeriodVal;
    u32 ulMaxCompareVal;
}tsPwmFnMapping;

// struct with registers for a GPIO pin
typedef struct _pin_mapping
{
    u32  ulPortStateRegister;
    u32  ulPortOutputDataRegister;
    u32  ulPortConfigRegister;
    u8   ucBitShift;
    u8   ucPort;
    bool bCheckIO;
} tPinMapping;

typedef struct
{
    pFunction pfPin[NUMBER_OF_MAX_PORT_PINS];      /* Callback function for the interrupt routine of Pins */
}tsCallbackIO;

/****************************************** Variables ****************************************************/
//Array holding registers for all sense pins
static const tPinMapping sSenseMap[COUNT_OF_SENSELINES]=
{
    #ifdef SENSE_MAP
        #define S_MAP(Senseline, PinMapping) PinMapping,
            SENSE_MAP
        #undef S_MAP
    #endif
};

//Array holding registers for all output pins
static const tPinMapping sOutputMap[COUNT_OF_OUTPUTS]=
{
    #ifdef OUTPUT_MAP
        #define O_MAP(Senseline, PinMapping) PinMapping,
            OUTPUT_MAP
        #undef O_MAP
    #endif
};

//Array holding functions for all PWM modules
static const tsPwmFnMapping sPwmMap[COUNT_OF_PWM] =
{
    #ifdef PWM_MAP
        #define P_MAP(Name, PwmFn) PwmFn,
            PWM_MAP
        #undef P_MAP
    #endif
};

/* Structure to hold information about the callbacks for each Port-Pin */
static const tsCallbackIO sCallbackIoMap[COUNT_OF_PORT_ISR] = 
{
    #ifdef ISR_IO_MAP
        #define ISR_MAP(ePort, pfPin_0, pfPin_1, pfPin_2, pfPin_3, pfPin_4, pfPin_5, pfPin_6, pfPin_7) {{pfPin_0, pfPin_1, pfPin_2, pfPin_3, pfPin_4, pfPin_5, pfPin_6, pfPin_7}},
            ISR_IO_MAP
        #undef ISR_MAP
    #endif
};


static u8 ucOutputPortMasks[NUMBER_OF_PORTS];
static bool bPortMasksInitialized = false;

/****************************************** Function prototypes ******************************************/
static void InitializePortMasks(void);
static u8 GetPortMask(u8 ucPort, bool bOutputMask);
static void GPIOInterruptHandler(void);


/****************************************** loacl functions *********************************************/
//********************************************************************************
/*!
\author     KraemerE   
\date       10.07.2018  

\fn         InitializePortMasks (void)

\brief      Initializes the port mask array and check it with a checksum.

\return     none - 
***********************************************************************************/
static void InitializePortMasks(void)
{
    u8 ucPortIdx = 0;
    u16 uiChecksum = 0;
    for(ucPortIdx = NUMBER_OF_PORTS; ucPortIdx--;)
    {
        ucOutputPortMasks[ucPortIdx] = GetPortMask(ucPortIdx, true);
        uiChecksum += ucOutputPortMasks[ucPortIdx];
    }
    
    /* Check for correct mask */
    if(uiChecksum && uiChecksum < (NUMBER_OF_PORTS*0xFF))
    {
        bPortMasksInitialized = true;
    }
}

//********************************************************************************
/*!
\author     KraemerE   
\date       10.07.2018  
\brief      Gets a specific port-mask
\return     ucPortMask - Returns the created port mask.
\param      ucPort - The port with the active outputs/inputs.
\param      bOutputMask - True when the output mask of this port should be get. True
                          when the intput mask should retrieved.
***********************************************************************************/
static u8 GetPortMask(u8 ucPort, bool bOutputMask)
{
    u8 ucPortMask = 0x00;    
    u8 ucIndex = 0;
    
    /* Get output pins only */
    if(bOutputMask)
    {        
        /* Get port mask for relays in this port */
        for(ucIndex = COUNT_OF_OUTPUTS; ucIndex--;)
        {
            /* Check for correct port and check for bit which describes if
               the IO can be checked */
            if(sOutputMap[ucIndex].ucPort == ucPort 
                && sOutputMap[ucIndex].bCheckIO == true)
            {
                ucPortMask |= 0x01 << sOutputMap[ucIndex].ucBitShift;
            }
        }
    }
    
    /* Get input pins only */
    else
    {
        /* Get port mask for digital sense lines in this port */
        for(ucIndex = COUNT_OF_SENSELINES; ucIndex--;)
        {
            /* Check for correct port and check for bit which describes if
               the IO can be checked */
            if(sSenseMap[ucIndex].ucPort == ucPort
                && sOutputMap[ucIndex].bCheckIO == true)
            {
                ucPortMask |= 0x01 << sSenseMap[ucIndex].ucBitShift;
            }
        }
    }
    return ucPortMask;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       03.10.2020
\fn         GPIOInterruptHandler
\brief      ISR function called by hardware whenever a GPIO detects an interrupt.
            Don't clear the specific interrupt request in the standby module.
            This handler is called when the critical section is left. When the IRQ
            is cleared before this module won't get any IRQs
\return     none
\param      none
***********************************************************************************/
static void GPIOInterruptHandler(void)
{            
    /* Get the interrupting port bitfields */
    u8 ucTriggeringPort = (*(reg32*)CYREG_GPIO_INTR_CAUSE);
    u8 ucPortIdx = 0;
    
    /* Process interrupts on each triggering port */
    while(ucTriggeringPort != 0)
    {
        /* Check if interrupt sticks on this port */
        if((ucTriggeringPort & 1) != 0)
        {
            /* Read interrupt status register for this port (includes copy of PS register in [23:16] */
            /* NOTE: platform-specific register offset calculation, but tighter flash usage */
            u32 ulInterruptStatus = (*(reg32 *)(CYREG_GPIO_PRT0_INTR | (ucPortIdx << 8)));            
            
            /* Check each port-pin for interrupt triggering */
            for(u8 ucPortPinIdx = 0; ucPortPinIdx < NUMBER_OF_MAX_PORT_PINS; ucPortPinIdx++)
            {
                if(ulInterruptStatus & (0x01 << ucPortPinIdx))
                {
                    /* Check first if a callback is used */
                    if(sCallbackIoMap[(teIoISR)ucPortIdx].pfPin[ucPortPinIdx])
                    {
                        sCallbackIoMap[(teIoISR)ucPortIdx].pfPin[ucPortPinIdx]();
                    }
                    else
                    {
                        /* Interrupt occured on a pin which hasn't set a callback. Use default error callback */
                        HAL_IO_ERRORCALLBACK
                    }
                }
            }
            
            /* Clear all pending interrupts on this port */
            *(reg32 *)(CYREG_GPIO_PRT0_INTR | (ucPortIdx << 8)) = 0xFF;            
        }
        
        /* Move to the next port */
        ucTriggeringPort >>= 1;
        ucPortIdx++;        
    }
}

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E   
\date       13.02.2019  
\brief      Initializes the Actors module. Actually only initializes the port mask
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Init(void)
{
    InitializePortMasks();
    
    /* Link All-Port-Isr to specific callback */
    AllPortIsr_StartEx(GPIOInterruptHandler);
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019  
\brief      Reads the state of the GPIO pin that are used for digital input.
\return     boolean - True for high and false for low
\param      eSenseLine - The sense line which should be read
***********************************************************************************/
bool HAL_IO_ReadDigitalSense(teSenseLine eSenseLine)
{    
    return CY_SYS_PINS_READ_PIN(sSenseMap[eSenseLine].ulPortStateRegister, sSenseMap[eSenseLine].ucBitShift);    
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Reads the state of the GPIO pin that are used for digital output.
\return     boolean - True for high and false for low
\param      eOutput - The output which should be read
***********************************************************************************/
bool HAL_IO_ReadOutputStatus(teOutput eOutput)
{    
    return CY_SYS_PINS_READ_PIN(sOutputMap[eOutput].ulPortStateRegister, sOutputMap[eOutput].ucBitShift);    
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Set the state of the GPIO pin that are used for digital output.
\return     boolean - True for high and false for low
\param      eSenseLine - The sense line which should be read
***********************************************************************************/
void HAL_IO_SetOutputStatus(teOutput eOutput, bool bStatus)
{   
    if(bStatus)
    {
        CY_SYS_PINS_SET_PIN(sOutputMap[eOutput].ulPortOutputDataRegister, sOutputMap[eOutput].ucBitShift);
    }
    else
    {
        CY_SYS_PINS_CLEAR_PIN(sOutputMap[eOutput].ulPortOutputDataRegister, sOutputMap[eOutput].ucBitShift);
    }  
}


//********************************************************************************
/*!
\author     Kraemer E      
\date       30.07.2018  
\brief      Reads the port pin state register of the choosen port
\return     ucPortDataRegisterValue - Value from the Register output
\param      ucPort - Portnumber which should be read.
***********************************************************************************/
u8 HAL_IO_ReadPortStateRegister(u8 ucPort)
{    
    u8 ucPortRegisterValue = 0;
    u32 ulDataRegisterAdress = 0;
    
    if (ucPort < NUMBER_OF_PORTS)
    {
        ulDataRegisterAdress = CYREG_GPIO_PRT0_PS + (CYDEV_GPIO_PRT0_SIZE * ucPort);
        ucPortRegisterValue = (u8)(*(reg32*)ulDataRegisterAdress);
    }
       
    return ucPortRegisterValue;
}


//********************************************************************************
/*!
\author     Kraemer E      
\date       30.07.2018  
\brief      Reads the port output data register of the choosen port
\return     ucPortDataRegisterValue - Value from the Register output
\param      ucPort - Portnumber which should be read.
***********************************************************************************/
u8 HAL_IO_ReadPortDataRegister(u8 ucPort)
{
    u8 ucPortRegisterValue = 0;
    u32 ulDataRegisterAdress = 0;
    
    if (ucPort < NUMBER_OF_PORTS)
    {
        ulDataRegisterAdress = CYREG_GPIO_PRT0_DR + (CYDEV_GPIO_PRT0_SIZE * ucPort);
        ucPortRegisterValue = (u8)(*(reg32*)ulDataRegisterAdress);
    }
    
    return ucPortRegisterValue;
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Returns the status of the port pin mask
\return     boolean - True for high and false for low
\param      none
***********************************************************************************/
bool HAL_IO_GetPortPinMaskStatus(void)
{
    return bPortMasksInitialized;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       14.05.2021   
\brief      Returns the address of the port-pin mask
\return     pucPinMaskAddr - Either the correct address is returned or NULL
\param      none
***********************************************************************************/
u8* HAL_IO_GetOutputPinMask(void)
{
    u8* pucPinMaskAddr = NULL;
    
    if(bPortMasksInitialized)
    {
        pucPinMaskAddr = &ucOutputPortMasks[0];
    }
    
    return pucPinMaskAddr;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       14.05.2021   
\brief      Saves the bit-shift index and the port index of the given output into
            the pointer.
\return     none
\param      eOutput - The output of which the port index and bitshift index shall
                      be retrieved.
\param      pucPortIdx - The address where the port index shall be saved
\param      pucBitshiftIdx - The address where the bit-shift index shall be saved
***********************************************************************************/
void HAL_IO_GetPortAndBitshiftIndex(teOutput eOutput, u8* pucPortIdx, u8* pucBitshiftIdx)
{
    /* Check first if output is valid */
    if(eOutput < eInvalidOutput)
    {
        //Check for valid pointer
        if(pucPortIdx)
        {
            /* Save port index of the output */
            *pucPortIdx = sOutputMap[eOutput].ucPort;
        }
        
        if(pucBitshiftIdx)
        {
            /* Save bit-shift index of the output */
            *pucBitshiftIdx = sOutputMap[eOutput].ucBitShift;
        }
    }
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       03.06.2019   
\brief      Changes the drive mode of the pins into high impedance analog
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Sleep(void)
{
    u8 ucPinIdx = 0;
    
    for(ucPinIdx = 0; ucPinIdx < _countof(sOutputMap); ucPinIdx++)
    {
        CY_SYS_PINS_CLEAR_PIN(sOutputMap[ucPinIdx].ulPortOutputDataRegister, sOutputMap[ucPinIdx].ucBitShift);
        CY_SYS_PINS_SET_DRIVE_MODE(sOutputMap[ucPinIdx].ulPortConfigRegister, sOutputMap[ucPinIdx].ucBitShift, CY_SYS_PINS_DM_ALG_HIZ);
    }
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       03.06.2019   
\brief      Changes the drive mode of the pins into initial drive mode before went
            to sleep
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Wakeup(void)
{
    
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       15.11.2020   
\brief      Checks if the PWM module is active
\return     bPwmEnabled - True when register bit is set otherwise false
\param      ucOutputIdx - The output which shall be checked
***********************************************************************************/
bool HAL_IO_GetPwmStatus(u8 ucOutputIdx)
{
    bool bPwmEnabled = false;
    
    switch(ucOutputIdx)
    {   
        #ifdef CY_TCPWM_PWM_0_H
            case 0: 
                bPwmEnabled = (PWM_0_BLOCK_CONTROL_REG & PWM_0_MASK) ? true : false;
                break;
        #endif
            
        #ifdef CY_TCPWM_PWM_1_H
            case 1: 
                bPwmEnabled = (PWM_1_BLOCK_CONTROL_REG & PWM_1_MASK) ? true : false;
                break;
        #endif
        
        #ifdef CY_TCPWM_PWM_2_H 
            case 2: 
                bPwmEnabled = (PWM_2_BLOCK_CONTROL_REG & PWM_2_MASK) ? true : false;
                break;   
        #endif    
            
        #ifdef CY_TCPWM_PWM_3_H 
            case 3:
                bPwmEnabled = (PWM_3_BLOCK_CONTROL_REG & PWM_3_MASK) ? true : false;
                break;
        #endif    
        
        default:
            break;
    }    
    
    return bPwmEnabled;
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Starts the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
\param      ePWM - The PWM module which shall be started
***********************************************************************************/
teIO_Return HAL_IO_PWM_Start(tePWM ePWM)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        sPwmMap[ePWM].pfnStart();
        eReturn = eIO_Success;        
    }    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Stops the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
\param      ePWM - The PWM module which shall be stopped
***********************************************************************************/
teIO_Return HAL_IO_PWM_Stop(tePWM ePWM)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        sPwmMap[ePWM].pfnStop();
        eReturn = eIO_Success;        
    }    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Reads the compare value the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a invalid pointer was used the return value 
                      is eIO_InvalidPointer.
\param      ePWM - The PWM module which shall be read from
\param      pulCompaVal - Pointer where the read value shall be saved to.
***********************************************************************************/
teIO_Return HAL_IO_PWM_ReadCompare(tePWM ePWM, u16* puiCompVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(puiCompVal)
        {
            *puiCompVal = sPwmMap[ePWM].pfnReadCompare();
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidPointer;
        }
    }
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Writes a compare value to the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a value greater than the max value is used the return value 
                      is eIO_InvalidValue.
\param      ePWM - The PWM module which shall be written to
\param      ulCompVal - The new compare value which shall be used. (Checks for max value)
***********************************************************************************/
teIO_Return HAL_IO_PWM_WriteCompare(tePWM ePWM, u32 ulCompVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(ulCompVal <= sPwmMap[ePWM].ulMaxCompareVal)
        {
            sPwmMap[ePWM].pfnWriteCompare(ulCompVal);
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidValue;
        }
    }
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Reads the period value from the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a invalid pointer was used the return value 
                      is eIO_InvalidPointer.
\param      ePWM - The PWM module which shall be read from
\param      pulCompaVal - Pointer where the read value shall be saved to.
***********************************************************************************/
teIO_Return HAL_IO_PWM_ReadPeriod(tePWM ePWM, u16* puiPeriodVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(puiPeriodVal)
        {
            *puiPeriodVal = sPwmMap[ePWM].pfnReadPeriod();
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidPointer;
        }
    }
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Writes a period value to the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a value greater than the max value is used the return value 
                      is eIO_InvalidValue.
\param      ePWM - The PWM module which shall be written to
\param      ulPeriodVal - The new period value which shall be used. (Checks for max value)
***********************************************************************************/
teIO_Return HAL_IO_PWM_WritePeriod(tePWM ePWM, u32 ulPeriodVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(ulPeriodVal <= sPwmMap[ePWM].ulMaxPeriodVal)
        {
            sPwmMap[ePWM].pfnWritePeriod(ulPeriodVal);
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidValue;
        }
    }
    return eReturn;
}
#endif //PSOC_4100S_PLUS