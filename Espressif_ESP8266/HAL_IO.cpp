/* ========================================
 *
 * Copyright Eduard Kraemer, 2019 *
 * ========================================
*/

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

#include "HAL_IO.h"
#include "Arduino.h"

/****************************************** Defines ******************************************************/
typedef struct _pwm_fn_mapping
{
    pFunction pfnStart;
    pFunction pfnStop;
    pFunctionParamU32 pfnWriteCompare;
    pulFunction pfnReadCompare;
    pulFunction pfnReadPeriod;
    pFunctionParamU32 pfnWritePeriod;
    u32 ulMaxPeriodVal;
    u32 ulMaxCompareVal;
}tsPwmFnMapping;

// struct with registers for a GPIO pin
typedef struct _pin_mapping
{
    u8 ucOutputPin;
} tPinMapping;

typedef struct
{
    pFunction pfPin[NUMBER_OF_MAX_PORT_PINS];      /* Callback function for the interrupt routine of Pins */
}tsCallbackIO;

/****************************************** Variables ****************************************************/
//Array holding registers for all sense pins
static const tPinMapping sSenseMap[COUNT_OF_SENSELINES]=
{
    #ifdef SENSE_MAP
        #define S_MAP(Senseline, PinMapping) PinMapping,
            SENSE_MAP
        #undef S_MAP
    #endif
};

//Array holding registers for all output pins
static const tPinMapping sOutputMap[COUNT_OF_OUTPUTS]=
{
    #ifdef OUTPUT_MAP
        #define O_MAP(Senseline, PinMapping) PinMapping,
            OUTPUT_MAP
        #undef O_MAP
    #endif
};

//Array holding functions for all PWM modules
static const tsPwmFnMapping sPwmMap[COUNT_OF_PWM] =
{
    #ifdef PWM_MAP
        #define P_MAP(Name, PwmFn) PwmFn,
            PWM_MAP
        #undef P_MAP
    #endif
};

/* Structure to hold information about the callbacks for each Port-Pin */
static const tsCallbackIO sCallbackIoMap[COUNT_OF_PORT_ISR] = 
{
    #ifdef ISR_IO_MAP
        #define ISR_MAP(ePort, pfPin_0, pfPin_1, pfPin_2, pfPin_3, pfPin_4, pfPin_5, pfPin_6, pfPin_7) {{pfPin_0, pfPin_1, pfPin_2, pfPin_3, pfPin_4, pfPin_5, pfPin_6, pfPin_7}},
            ISR_IO_MAP
        #undef ISR_MAP
    #endif
};

/****************************************** Function prototypes ******************************************/

/****************************************** loacl functions *********************************************/


/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E   
\date       13.02.2019  
\brief      Initializes the Actors module. Actually only initializes the port mask
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Init(void)
{
    //Set output mode for all output pins
    for(int OutputIdx = 0; OutputIdx < COUNT_OF_OUTPUTS; OutputIdx++)
    {
        pinMode(sOutputMap[OutputIdx].ucOutputPin, OUTPUT);
    }
}

#if COUNT_OF_SENSELINES
//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019  
\brief      Reads the state of the GPIO pin that are used for digital input.
\return     boolean - True for high and false for low
\param      eSenseLine - The sense line which should be read
***********************************************************************************/
bool HAL_IO_ReadDigitalSense(teSenseLine eSenseLine)
{    
    return true;   
}
#endif //COUNT_OF_SENSELINES

#if COUNT_OF_OUTPUTS
//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Reads the state of the GPIO pin that are used for digital output.
\return     boolean - True for high and false for low
\param      eOutput - The output which should be read
***********************************************************************************/
bool HAL_IO_ReadOutputStatus(teOutput eOutput)
{    
    return digitalRead(sOutputMap[eOutput].ucOutputPin);  
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Set the state of the GPIO pin that are used for digital output.
\return     boolean - True for high and false for low
\param      eSenseLine - The sense line which should be read
***********************************************************************************/
void HAL_IO_SetOutputStatus(teOutput eOutput, bool bStatus)
{   
    digitalWrite(sOutputMap[eOutput].ucOutputPin, bStatus);
}
#endif //COUNT_OF_OUTPUTS

//********************************************************************************
/*!
\author     Kraemer E      
\date       30.07.2018  
\brief      Reads the port pin state register of the choosen port
\return     ucPortDataRegisterValue - Value from the Register output
\param      ucPort - Portnumber which should be read.
***********************************************************************************/
u8 HAL_IO_ReadPortStateRegister(u8 ucPort)
{    
    return 0xFF;
}


//********************************************************************************
/*!
\author     Kraemer E      
\date       30.07.2018  
\brief      Reads the port output data register of the choosen port
\return     ucPortDataRegisterValue - Value from the Register output
\param      ucPort - Portnumber which should be read.
***********************************************************************************/
u8 HAL_IO_ReadPortDataRegister(u8 ucPort)
{
    return 0xFF;
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       12.02.2019   
\brief      Returns the status of the port pin mask
\return     boolean - True for high and false for low
\param      none
***********************************************************************************/
bool HAL_IO_GetPortPinMaskStatus(void)
{
    return true;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       14.05.2021   
\brief      Returns the address of the port-pin mask
\return     pucPinMaskAddr - Either the correct address is returned or NULL
\param      none
***********************************************************************************/
u8* HAL_IO_GetOutputPinMask(void)
{   
    return NULL;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       14.05.2021   
\brief      Saves the bit-shift index and the port index of the given output into
            the pointer.
\return     none
\param      eOutput - The output of which the port index and bitshift index shall
                      be retrieved.
\param      pucPortIdx - The address where the port index shall be saved
\param      pucBitshiftIdx - The address where the bit-shift index shall be saved
***********************************************************************************/
void HAL_IO_GetPortAndBitshiftIndex(teOutput eOutput, u8* pucPortIdx, u8* pucBitshiftIdx)
{
    /* Check first if output is valid */
    if(eOutput < eInvalidOutput)
    {
        //Check for valid pointer
        if(pucPortIdx)
        {
            /* Save port index of the output */
            *pucPortIdx = 0;
        }
        
        if(pucBitshiftIdx)
        {
            /* Save bit-shift index of the output */
            *pucBitshiftIdx = 0;
        }
    }
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       03.06.2019   
\brief      Changes the drive mode of the pins into high impedance analog
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Sleep(void)
{
    //TODO
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       03.06.2019   
\brief      Changes the drive mode of the pins into initial drive mode before went
            to sleep
\return     none
\param      none
***********************************************************************************/
void HAL_IO_Wakeup(void)
{
    //TODO
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       15.11.2020   
\brief      Checks if the PWM module is active
\return     bPwmEnabled - True when register bit is set otherwise false
\param      ucOutputIdx - The output which shall be checked
***********************************************************************************/
bool HAL_IO_GetPwmStatus(u8 ucOutputIdx)
{
    bool bPwmEnabled = false;
    
    return bPwmEnabled;
}

#if COUNT_OF_PWM
//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Starts the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
\param      ePWM - The PWM module which shall be started
***********************************************************************************/
teIO_Return HAL_IO_PWM_Start(tePWM ePWM)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        sPwmMap[ePWM].pfnStart();
        eReturn = eIO_Success;        
    }    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Stops the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
\param      ePWM - The PWM module which shall be stopped
***********************************************************************************/
teIO_Return HAL_IO_PWM_Stop(tePWM ePWM)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        sPwmMap[ePWM].pfnStop();
        eReturn = eIO_Success;        
    }    
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Reads the compare value the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a invalid pointer was used the return value 
                      is eIO_InvalidPointer.
\param      ePWM - The PWM module which shall be read from
\param      pulCompaVal - Pointer where the read value shall be saved to.
***********************************************************************************/
teIO_Return HAL_IO_PWM_ReadCompare(tePWM ePWM, u16* puiCompVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(puiCompVal)
        {
            *puiCompVal = sPwmMap[ePWM].pfnReadCompare();
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidPointer;
        }
    }
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Writes a compare value to the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a value greater than the max value is used the return value 
                      is eIO_InvalidValue.
\param      ePWM - The PWM module which shall be written to
\param      ulCompVal - The new compare value which shall be used. (Checks for max value)
***********************************************************************************/
teIO_Return HAL_IO_PWM_WriteCompare(tePWM ePWM, u32 ulCompVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(ulCompVal <= sPwmMap[ePWM].ulMaxCompareVal)
        {
            sPwmMap[ePWM].pfnWriteCompare(ulCompVal);
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidValue;
        }
    }
    return eReturn;
}

//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Reads the period value from the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a invalid pointer was used the return value 
                      is eIO_InvalidPointer.
\param      ePWM - The PWM module which shall be read from
\param      pulCompaVal - Pointer where the read value shall be saved to.
***********************************************************************************/
teIO_Return HAL_IO_PWM_ReadPeriod(tePWM ePWM, u16* puiPeriodVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(puiPeriodVal)
        {
            *puiPeriodVal = sPwmMap[ePWM].pfnReadPeriod();
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidPointer;
        }
    }
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E   
\date       05.04.2021   
\brief      Writes a period value to the chosen PWM module.
\return     eReturn - eIO_Success when a valid PWM is used. Otherwise eIO_InvalidPWM.
                      When a value greater than the max value is used the return value 
                      is eIO_InvalidValue.
\param      ePWM - The PWM module which shall be written to
\param      ulPeriodVal - The new period value which shall be used. (Checks for max value)
***********************************************************************************/
teIO_Return HAL_IO_PWM_WritePeriod(tePWM ePWM, u32 ulPeriodVal)
{
    teIO_Return eReturn = eIO_InvalidPWM;
    
    if(ePWM < eInvalidPWM)
    {
        if(ulPeriodVal <= sPwmMap[ePWM].ulMaxPeriodVal)
        {
            sPwmMap[ePWM].pfnWritePeriod(ulPeriodVal);
            eReturn = eIO_Success; 
        }
        else
        {
            eReturn = eIO_InvalidValue;
        }
    }
    return eReturn;
}
#endif //COUNT_OF_PWM
#endif //ESP_8266